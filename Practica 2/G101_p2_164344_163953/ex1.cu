#include "stdio.h"
#define N 100


__global__ void add(int *a, int *b, int *c)
{

int tid=threadIdx.x+blockIdx.x*blockDim.x;

if (tid < N)
c[tid] = a[tid] + b[tid];
}

static int a[N], b[N], c[N];

int main()
{

	int *dev_a, *dev_b, *dev_c;

	cudaMalloc((void**)&dev_a, N * sizeof(int) );
	cudaMalloc((void**)&dev_b, N * sizeof(int) );
	cudaMalloc((void**)&dev_c, N * sizeof(int) );

	for (int i = 0; i < N; i++){
		a[i] = i,
		b[i] = 1;
	}

	cudaMemcpy(dev_a, a, N*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(dev_b, b, N*sizeof(int), cudaMemcpyHostToDevice);



		//add<<<1,N>>>(dev_a,dev_b,dev_c);

	
	     	add<<<N,1>>>(dev_a,dev_b,dev_c);
	     


	cudaMemcpy(&c, dev_c, N*sizeof(int), cudaMemcpyDeviceToHost);//Copy memory from device to host

	for (int i = 0; i < N; i++)
		printf("%d + %d = %d\n", a[i], b[i], c[i]);
	
	return 0;

}
