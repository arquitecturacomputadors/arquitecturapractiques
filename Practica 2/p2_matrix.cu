#include <stdio.h> 
#include <stdlib.h> 

__global__ void fill_matrix_device(int *m, int width) 
{ 
    int tx=threadIdx.x; 
    int ty=threadIdx.y; 
    
    int value=(tx+1)*(ty+1); 
    m[tx*width+ty] = value; 
}

void fill_matrix_host(int *m, int width) 
{ 
    for(int x=0;x<width;++x) { 
        for(int y=0;y<width;++y) { 
            int value=(x+1)*(y+1); 
            m[x*width+y] = value; 
        } 
    } 
} 

__global__ void matrix_mult_device(int *Ma, int *Mb, int *Mc, int width){

    int row = blockIdx.y*blockDim.y + threadIdx.y;
    int col = blockIdx.x*blockDim.x + threadIdx.x;

    float sum = 0.f;
    for (int n = 0; n < width; ++n)
        sum += Ma[row*width+n]*Mb[n*width+col];

    Mc[row*width+col] = sum;

}

int main(void) 
{ 
    int width=2; 
    int size=width*width*sizeof(int); 

    int *m; 
    m = (int *)malloc(size); 
 
    fill_matrix_host(m, width); 
    
    int *dev_m; 
    cudaMalloc((void **)&dev_m, size); 
    dim3 dimGrid(1, 1); 
    dim3 dimBlock(width, width); 
    
    int *dev_ma; 
    m = (int *)malloc(size); 
    int *dev_mb; 
    m = (int *)malloc(size); 
    int *dev_mc; 
    m = (int *)malloc(size); 

    int *Mc; 
    cudaMalloc((void **)&dev_mc, size); 

    fill_matrix_device<<<dimGrid, dimBlock>>>(dev_m, width); 
    fill_matrix_device<<<dimGrid, dimBlock>>>(dev_ma, width); 
    fill_matrix_device<<<dimGrid, dimBlock>>>(dev_mb, width); 

    matrix_mult_device<<<dimGrid,dimBlock>>>(dev_ma,dev_mb,dev_mc,width);

    int *m2; 
    m2 = (int *)malloc(size); 
    
    cudaMemcpy(m2, dev_m, size, cudaMemcpyDeviceToHost); 
    cudaMemcpy(Mc, dev_mc, size, cudaMemcpyDeviceToHost); 
    cudaMemcpy(Mc, dev_mc, size, cudaMemcpyDeviceToHost); 
    
    int ok=1; 
    for(int i=0;i<(width*width);++i) { 
        if(m[i]!=m2[i]) ok=0; 
    } 
    for(int i=0;i<(width*width);++i) { 
        if(Mc[i]!=Mc[i]) ok=0; 
    } 
    fprintf(stdout, "%s\n", ok?"ok":"error"); 
    
    free(m); 
    free(m2); 
    cudaFree(m); 
    
    return 0; 
}


