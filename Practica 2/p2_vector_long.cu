#include "stdio.h"
#include "math.h"
#define N 67107840

static int a[N], b[N], c[N];


__global__ void add(int *a, int *b, int *c){

	int tid = threadIdx.x + blockIdx.x%65535 * blockDim.x;

	if(tid < N)
		c[tid] = a[tid] + b[tid];
}

int main(){
	int *dev_a, *dev_b, *dev_c;

	cudaMalloc( (void**)&dev_a, N * sizeof(int) );					//Reservem al memória de la GPU, s'han de reservar totes
    cudaMalloc( (void**)&dev_b, N * sizeof(int) );
	cudaMalloc( (void**)&dev_c, N * sizeof(int) ); 

	for (int i = 0; i < N; i++){
		a[i] = i,
		b[i] = 1;
	}

	cudaMemcpy(dev_a, a, N * sizeof(int), cudaMemcpyHostToDevice); 	//Dela CPU A LA GPU
	cudaMemcpy(dev_b, b, N * sizeof(int), cudaMemcpyHostToDevice);

	if (N < 1024)
		add<<<1, N>>>(dev_a, dev_b, dev_c);

	else if (N < 65535)
		add<<<N, 1>>>(dev_a, dev_b, dev_c);

	else if (N < 67107840)
		add<<<65535, (int)N/65535>>>(dev_a, dev_b, dev_c);

	else 
		add<<<65535, 1024>>>(dev_a, dev_b, dev_c);

	cudaMemcpy( c, dev_c, N * sizeof(int), cudaMemcpyDeviceToHost );

	for (int i = 0; i < N; i++)
		printf("%d + %d = %d\n", a[i], b[i], c[i]);

	cudaFree (dev_a); 
	cudaFree (dev_b); 
	cudaFree (dev_c);

	return 0;
}
