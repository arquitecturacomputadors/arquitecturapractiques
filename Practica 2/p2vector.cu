#include "stdio.h"
#include "math.h"
#define N 65534

__global__ void add(int *a, int *b, int *c)
{
	int tid;

	if (N < 1024)
		tid = threadIdx.x; 

	else if (N < 65535)
		tid = blockIdx.x;

		c[tid] = a[tid] + b[tid];
}

int main()
{
	int a[N], b[N], c[N];
	int *dev_a, *dev_b, *dev_c;

	cudaMalloc( (void**)&dev_a, N * sizeof(int) );					//Reservem al memória de la GPU, s'han de reservar totes
    cudaMalloc( (void**)&dev_b, N * sizeof(int) );
	cudaMalloc( (void**)&dev_c, N * sizeof(int) ); 

	for (int i = 0; i < N; i++){
		a[i] = i,
		b[i] = 1;
	}

	cudaMemcpy(dev_a, a, N * sizeof(int), cudaMemcpyHostToDevice); 	//Dela CPU A LA GPU
	cudaMemcpy(dev_b, b, N * sizeof(int), cudaMemcpyHostToDevice);

	if (N < 1024)
		add<<<1, N>>>(dev_a, dev_b, dev_c); //<<<Threads, Blocks>>>

	else if (N < 65535)
		add<<<N, 1>>>(dev_a, dev_b, dev_c); //<<<Threads, Blocks>>>

	cudaMemcpy( c, dev_c, N * sizeof(int), cudaMemcpyDeviceToHost );//Copy memory from device to host

	for (int i = 0; i < N; i++)
		printf("%d + %d = %d\n", a[i], b[i], c[i]);

	cudaFree (dev_a); 
	cudaFree (dev_b); 
	cudaFree (dev_c);

	return 0;
}
