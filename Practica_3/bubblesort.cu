#include <stdio.h>
#include <time.h>


static const int N = 15;

__device__ void swap(int *elem1, int *elem2){
	int temp = *elem1;
	*elem1 = *elem2;
	*elem2 = temp;
}

__global__ void bubble_sort(int *array, int iteracio)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;

	//Condició per a continuar iterant
	if(idx < iteracio)
		if(*(array + idx) > *(array + (idx+1)))
			swap((array + idx), (array + (idx+1)));	
}



int main(int argc, char const *argv[]) 
{
	
    srand(time(NULL));


	int a[N];
	int *dev_a;


	cudaMalloc((void**)&dev_a, (N*N) * sizeof(int));

	for(int i=0;i<N;i++)
		a[i] = (int)rand()/(int)(RAND_MAX/100.0);

	for(int i=0;i<N;i++)
		printf("Unsorted: %d \n", a[i]);

	cudaMemcpy(dev_a, a, N * sizeof(int), cudaMemcpyHostToDevice);

	for (int it = 0; it <= N*2; it++) {

		bubble_sort<<<1,N>>>(dev_a, it);
	}

	cudaMemcpy(a, dev_a, N * sizeof(int), cudaMemcpyDeviceToHost);


	for(int i=0;i<N;i++)
		printf("Sorted: %d \n", a[i]);


	cudaFree(dev_a);
	
	return 0;
}
