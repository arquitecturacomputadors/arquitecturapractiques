#include <stdio.h>
#include <time.h>

static const int N = 15;

__global__ void cerca_array_device(int *array, int *valor,int *res)
{

    int b = N;
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    for(int i = 0; i <= 1000000; i++)b = (b*70)/3;
    if(array[id] == (*valor))
        *res = 1;
}


__host__ bool cerca_array_host(int *array, int valor)
{

    int b = N;
    for(int i = 0; i <= 1000000; i++)b = (b*70)/3;
	for(int i = 0 ; i < N ; i++){
        if(array[i] == valor){
            printf("TROBAT!");
            return true;
        }
    }   
    printf("NO TROBAT!");
    return false;
}

int main()
{

    srand(time(NULL));

    int *a,valor = 0;
    int *dev_a, *dev_res, *dev_valor;
    int res = -1;


    a = (int*)malloc(N*sizeof(int));
    cudaMalloc((void**)&dev_a,  N * sizeof(int));
    cudaMalloc((void**)&dev_res,    sizeof(int));
    cudaMalloc((void**)&dev_valor,  sizeof(int));

    for(int i=0;i<N;i++)
    	a[i] = (int)rand()/(int)(RAND_MAX/300.0);

    for(int i=0;i<N;i++)
        printf("valor: %d \t", a[i]);

   
    printf("\nNombre a cercar: ");
    scanf("%d",&valor);


    //Execució a la CPU
    clock_t t_host = clock();
    cerca_array_host(a,valor);
    t_host = clock() - t_host;
    double time_taken_host = ((double)t_host)/CLOCKS_PER_SEC;
    printf("CPU: %f segons \n", time_taken_host);

    cudaMemcpy(dev_a, a,      N * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dev_res, &res,     sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(dev_valor, &valor, sizeof(int), cudaMemcpyHostToDevice);

    clock_t t_device = clock();
	cerca_array_device<<<1,N>>>(dev_a, dev_valor, dev_res);
    t_device = clock() - t_device;    
    double time_taken_device = ((double)t_device)/CLOCKS_PER_SEC; 
    printf("GPU %f segons \n", time_taken_device);

    cudaMemcpy(&res, dev_res, sizeof(int), cudaMemcpyDeviceToHost);

	//Printa si ha cercat el nombre
    if(res == 1)
        printf("El nombre s'ha trobat");
    else 
        printf("El nombre no s'ha trobat");

    cudaFree(dev_a);
    cudaFree(dev_res);
    cudaFree(dev_valor);
   
return 0;


}

