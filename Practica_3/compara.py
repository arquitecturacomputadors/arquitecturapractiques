import math, operator
from PIL import Image
def compare():
	image1 = Image.open("output_cuda.bmp")
	image2 = Image.open("output_host.bmp")
	h1 = image1.histogram()
	h2 = image2.histogram()
	rms = math.sqrt(reduce(operator.add,map(lambda a,b: (a-b)**2, h1, h2))/len(h1))
	return rms

if __name__=='__main__':
	import sys
	print compare()