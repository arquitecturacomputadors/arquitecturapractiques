#include <stdio.h>
#include <stdlib.h>

//static const int N = 16;
static const int N = 32;
//static const int N = 13;
//...


//Kernel que distribueix la l'execució a la grid
__global__ void organitza_grid(int *array) {

    int idx_x = threadIdx.x + blockIdx.x * blockDim.x;
    int idx_y = threadIdx.y + blockIdx.y * blockDim.y; 

    //Distribueix la grid(blocks i threads) com a un array unidimensional i calcula l'index d'aquesta distribució. 
    //On cada index correspon a un thread de la grid
    int gridWidth = gridDim.x * blockDim.x;
    int idx  = idx_y * gridWidth + idx_x;

    
    //Recupera l'index del block a la grid
    int idx_r = blockIdx.y * gridDim.x + blockIdx.x +1;

    //Guarda resultad al array

	array[idx] = idx_r;
}


__host__ void printa(int *array)
{

    for(int i = 0; i < N; ++i)
    {
        for(int j = 0; j < N; ++j)
            printf("%2d ", array[i * N + j]);
        printf("\n");
    }
    printf("\n");
}


int main(void) {

    int *dev_a;
    int *array;

    // Reserva memoria al host i al device
    array = (int*)malloc((N*N)*sizeof(int));
    cudaMalloc((void**)&dev_a, (N*N) * sizeof(int));

    if(array == 0 || dev_a == 0)
    {
        printf("couldn't allocate memory\n");
        return 1;
    }
    //Crea blocks de dos dimensions amb diferent nombre de threads. Ex: Comença amb 4x4
    dim3 block_dim;
    block_dim.x = 4;
    block_dim.y = 4;
	

    // Crea i inicialitza una grid en 2 dimensions
    dim3 grid_dim;
    grid_dim.x = N / block_dim.x;
    grid_dim.y = N / block_dim.y;

    organitza_grid<<<grid_dim, block_dim>>>(dev_a);
    cudaMemcpy(array, dev_a, (N*N) * sizeof(int), cudaMemcpyDeviceToHost);


    // Printa els resultats de l'organització de la grid
    printa(array);
   
    free(array);
    cudaFree(dev_a);


    return 0;
}
