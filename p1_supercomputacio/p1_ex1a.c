#include <stdlib.h>
#include <stdio.h>
#include <time.h>

/*

	Allocate memory for an array of ints and resize it dynamically

*/



int main(void)
{


	srand(time(NULL));

	printf("Array size: ");
	int size = 0;
	scanf("%d", &size);

	// Allocate memory for an array of size elements
	int *array = (int *)malloc(size * sizeof(int));

	//Fill array elements using random values. Acces to array position adress. Not by value: (NOT array[i])
	for (int i = 0; i < size; ++i)
	{
		*(array+i) = (int)rand()/(int)(RAND_MAX/50.0);
		// Print the array elements using pointers to each array position. (NOT array[i]) 
		//Your code here
		printf("%d ", *(array+i));
	}

	// User specifies the new array size, stored in variable n2.
	printf("\nNew array size: ");
	int new_size = 0;
	scanf("%d", &new_size);
	// Change the array size dynamically using realloc()

	array = (int *)realloc(array, new_size * sizeof(int));

	//If the new size is larger, set all members to 0. Why?
	if(new_size > size){
		printf("%d\n", *array);
		for (int i = size; i < new_size; ++i)
			*(array+i) = 0;
	}
	// Print the resized array elements using pointers to each array position. (NOT array[i]) 

	for (int i = 0; i < new_size; ++i)
		printf("%d ",*(array+i));
	
	//Set memory free
	free(array);

	return 0;



}

