#include <stdio.h> 
#include <stdlib.h>
#include <time.h>

static const int SIZE = 15;


//swap values at memory locations
	void swap(int *elem1, int *elem2)
{
	int temp = *elem1;
	*elem1 = *elem2;
	*elem2 = temp;

}


//Bubble sort algorithm to sort arrays in ascending order
void bubbleSort(int * const array, const int size)
{
	int i, j;

	for (i = 1; i < size; i++) 
	  for (j = 0; j < size - 1; j++) 
	     if (*(array + j) > *(array +(j + 1)))
	     	//2. swap adjacent elements if they are out of order
	     	swap((array + j), array +(j + 1));
	  
}

int main(void)
{

    srand(time(NULL));
	//Initialize an array of size SIZE with random values
	int a[SIZE];

	for(int i=0;i<SIZE;i++)
		a[i] = (int)rand()/(int)(RAND_MAX/60.0);
  

	printf("Print original values\n");

	//Your code here
	for (int i = 0; i < SIZE; ++i)
		printf("%d ", a[i]);
	printf("\n");

	printf("Print ordered values\n");
	bubbleSort(a, SIZE);
	for (int i = 0; i < SIZE; ++i)
		printf("%d ", a[i]);

	return 0;




}
