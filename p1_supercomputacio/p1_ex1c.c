#include <stdio.h>
#include <stdlib.h>

/*
	Cerca de d'arbres binaris amb funcions recursives
*/


//Struct per arbre binary. Cada node pot tenir fins a dos fills(esq, dret). Cada node conte un enter.
struct arbreBinari
{
	int contingut;
	struct arbreBinari* esq;
	struct arbreBinari* dret;
};


//Reserva memoria per l'arbre en funció dels nodes que s'afegeixen
struct arbreBinari* reserva_memoria(int contingut)
{
	//Reserva memoria
	struct arbreBinari* arbre = (arbreBinari *)malloc(sizeof(struct arbreBinari));
	//Inicialitzem el node del arbre amb el contingut i els fills a NULL
	arbre->contingut = contingut;
	arbre->dret = NULL;
	arbre->esq = NULL;

	return (arbre);


}

//Afegeix un node a l'arbre amb un contingut de tipus int
struct arbreBinari* afegeix_node(struct arbreBinari* arrel, int contingut)
{

	//Condició de sortida

	if(arrel==NULL){
		//1.Reserva memoria per el node
		//2.Retorna la nova arrel
		return reserva_memoria(contingut);	

	}
	//Crida recursiva a afegeix_node segons el valor de contingut
	else if(contingut < arrel->contingut)
		arrel->esq = afegeix_node(arrel->esq, contingut);
	else
		arrel->dret = afegeix_node(arrel->dret, contingut);



}

//Cerca en pre-order
void pre_order(struct arbreBinari* arrel)
{

	//1.Condició de sortida
	//2.Imprimeix contingut
	//3.Crida recursiva
	if (arrel != NULL) {
	    printf("%d ", arrel->contingut);                     
	    pre_order(arrel->esq);
	    pre_order(arrel->dret);
  	}
	

}

//Cerca en in-order. 
void in_order(struct arbreBinari* arrel)
{

	//1.Condició de sortida
	//2. Imprimeix contingut
	//3. Crida recursiva
	if (arrel != NULL) {
	    in_order(arrel->esq);
	    printf("%d ", arrel->contingut);
	    in_order(arrel->dret);
  	}	

}


//Allibera la memoria reservada previament i retorna el nombre de nodes esborrats
int esborra_arbre(struct arbreBinari* arrel)
{

	int count = 0;
	//1.Condició de sortida
	//2. Esborrar cada node recursivament
	//3.Allibera memoria de cada node 
	if(arrel!=NULL){
		//Recorrem l'arbre en postr-ordre i anem borrant els nodes 
		count += esborra_arbre(arrel->esq);
		count += esborra_arbre(arrel->dret);
		free(arrel);
	}
	else
		--count;

	return ++count;


}


int main()
{

	//Inicialitzem un arbre buit
	struct arbreBinari* arrel = NULL;
	//Afegeix els valors(4,1,0,3,6,5,4,9)
	arrel = afegeix_node(arrel, 4);
	arrel = afegeix_node(arrel, 1);
	arrel = afegeix_node(arrel, 0);
	arrel = afegeix_node(arrel, 3);
	arrel = afegeix_node(arrel, 6);
	arrel = afegeix_node(arrel, 5);
	arrel = afegeix_node(arrel, 4);
	arrel = afegeix_node(arrel, 9);

	//Cridem les funcions de cerca
	pre_order(arrel);
	printf("\n");
	in_order(arrel);
	printf("\n");
	
	int count = 0;
	//Alliberem la memoria reservada per cada node
	count = esborra_arbre(arrel);


	arrel=NULL;

	//Nombre total de nodes esborrats
	printf("Nodes esborrats: %d", count);

	return 0;



}







