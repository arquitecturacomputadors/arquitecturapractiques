#include <stdio.h> 
#include <stdlib.h>
#include <time.h>


//Multiply two given matrixs
void multiply_matrix(float *A, float *B, float *M, int width, int pos) 
{ 
	
    //Calculate the row and column given position pos
    int row = pos/width; //.....
    int col = pos % width; //.....   
    *(M + pos) = (*(A + (row * width))) * (*(B + col)) + (*(A + ((row * width) + 1))) * (*(B + (col + width))); 
}

//Calculate de matrix determinant
void matrix_determinant(float *M, float *D, int width)
{
  
  *D = (*(M)) * (*(M + (width * width-1))) - (*(M + 1)) * (*(M + width));
  
}



//Print a matrix M
void display_matrix(float *M, int width)
{

    for (int i = 0; i < (width*width); ++i)
    {
        printf("%f  ", *(M + i));
        if(i == 1)
            printf("\n");
    }
    printf("\n");

} 


int main(void) 
{ 
    int width=2; 
    srand(time(NULL));
    
    //Allocate the memory
    float *A = (float *)malloc((width * width) * sizeof(float)); //..... 
    float *B = (float *)malloc((width * width) * sizeof(float)); //..... 
    float *M  = (float *)malloc((width * width) * sizeof(float)); //..... 

    for(int i=0;i<(width*width);i++) { 
      
    	A[i] = (float)rand()/(float)(RAND_MAX/15.0);
    	B[i] = (float)rand()/(float)(RAND_MAX/15.0);
        
    } 
    display_matrix(A, width);
    printf("\n");
    display_matrix(B, width);
    printf("\n");

    for(int i=0;i<(width*width);i++) { 
        multiply_matrix(A, B, M, width, i); 
    } 
    
    
    //Calculate the matrix determinant
    float D;
    printf("\n");
    matrix_determinant(M, &D, width);   
    printf("Determinant is: %f\n", D);
    

    fprintf(stdout, "Check that M = A * B\n");
    display_matrix(M, width);
    //Display A, B and M
    //Set memory free
    free(A);
    free(B);
    free(M);
    return 0; 
}
